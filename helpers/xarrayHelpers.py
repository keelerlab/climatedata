import rasterio as rio
from scipy.io import loadmat
import numpy as np
import xarray as xr
import geopandas as geo
import warnings
import pandas as pd
from shapely.geometry import mapping
import rioxarray
import sys
import os

# TODO Figure out if this is a fix to the runtime warning. RuntimeWarning: invalid value encountered in true_divide
#   x = np.divide(x1, x2, out)
np.seterr(divide='ignore', invalid='ignore')

def array2geotiff(array, templateTiff, outputFile='output.tif'):
    # TODO: These are notes from initial meeting
    # project first and then mask after. note that terin never projected and you will have to.
    # need to do a resampling method of bilinear and the cell size we haven't decided but could use 8100 x 8100
    # for whenever this may or may not come up. ## yays
    # mask to Minnesota as an option. There are times when we will want to say,
    # this masking for minnesota, should h
    # Really this should be a optional shape file or something. that would be a generic form.
    if array.dtype == 'int64':
        array = array.astype('float64')

    with rio.open(templateTiff) as src:
        naip_meta = src.profile

    # This is true
    naip_meta['dtype'] = str(array.dtype)

    with rio.open(outputFile, 'w', **naip_meta) as dst:
        dst.write(array, 1)

def fixLat(dataset_list):
    """ Fix the latitude to be the same for all of them. ASSUMES that there would only be 2 groups of messed up things """
    # Determine which of the models are all the same
    model_ind_0 = []
    model_ind_1 = []
    for i in range(len(dataset_list)):
        if len(set(dataset_list[0]['lat'].values) - set(dataset_list[i]['lat'].values)) == 0:
            model_ind_0.append(i)
        else:
            model_ind_1.append(i)

    if len(model_ind_0) == 0 or len(model_ind_1) == 0:
        print("NOTE: They all match up, no lat fixing needed")
        return dataset_list
    print("NOTE: Had to fix the latitude to match up")

    if len(model_ind_1) > len(model_ind_0):
        for model_index in model_ind_0:
            dataset_list[model_index] = dataset_list[model_index].assign_coords(lat=dataset_list[model_ind_1[0]]['lat'])
    else:
        for model_index in model_ind_1:
            dataset_list[model_index] = dataset_list[model_index].assign_coords(lat=dataset_list[model_ind_0[0]]['lat'])

    return dataset_list

def returnOldMasked(dataSet):
    """ From the report in September, a specific set of masks were done that will be utilized here. """

    ret = mask(dataSet, 10, 10, 10, 10)
    my_path = os.path.abspath(os.path.dirname(__file__))
    ret = mask_from_mat(ret, os.path.join(my_path, '../spatial/implausible5day.mat'), 'implausible5day', value=0)
    ret = mask_from_mat(ret, os.path.join(my_path, '../spatial/WRFmask_e7.mat'), 'WRFmask_e7')
    ret = mask_from_mat(ret, os.path.join(my_path, '../spatial/mnOnlyMask.mat'), 'mnOnlyMask')

    return ret

def mask(dataSet, left=5, right=5, top=5, bottom=5):
    """ Implements a max of the certain level on all sides Assumes Lat Lon are the mask
    https://geohackweek.github.io/nDarrays/09-masking/"""

    ## A little error checking to make sure that you're giving it values that work.
    assert (left > 0 and right > 0 and top > 0 and bottom > 0)
    assert (int(left) == left and int(right) == right and int(top) == top and int(bottom) == bottom)

    mask_array = np.ones((dataSet['lon'].size, dataSet['lat'].size))
    mask_array[0:top, :] = 0  # First 5 rows
    mask_array[-bottom:, :] = 0  # Last 5 rows
    mask_array[:, 0:left] = 0  # First 5 cols
    mask_array[:, -right:] = 0  # Last 5 cols

    dataSet.coords['mask'] = (('lon', 'lat'), mask_array)
    return dataSet.where(dataSet.mask == 1).drop_vars('mask')

def mask_from_mat(dataSet, file_name, matrix_name, value=1):
    """ Takes a matlab input file and then works with that to create a mask.
        Matrix is assumed to be 1 where the implausible values should be removed.
    """
    mask = loadmat(file_name)
    mask_vals = mask.get(matrix_name)
    dataSet.coords['mask'] = (('lon', 'lat'), mask_vals)
    return dataSet.where(dataSet.mask == value).drop_vars('mask')

    # TODO Create a new output function that will output data like this.
    # swap modelers will need daily data for a particular place or small number of places for several variables and need years of daily data
    # geotiffs are poor set up for that
    # netcdfs are bad
    # need the tabular data so as in output.

def find_longest_consecutive(array, threshold, above, returnStartIndex=False):
    """helper that if you give it an array it'll output the longest sequence of values
        Assumes, 0, 1, nan"""
    if above:
        threshold = array > threshold
    else:
        threshold = array < threshold

    my_lst_str = ''.join(map(lambda x: str(int(x)), threshold))
    start_index = max(my_lst_str.split('0'), key=len)
    if returnStartIndex:
        return my_lst_str.find(start_index)
    return len(start_index)

def longest_consecutive(data, threshold=0.2541, above=True, start_date=False):
    return xr.apply_ufunc(find_longest_consecutive,
                          data,
                          input_core_dims=[["time"]],
                          kwargs={
                              'threshold': threshold,
                              'above': above,
                              'returnStartIndex': start_date
                          },
                          dask='allowed',
                          vectorize=True)

def find_sum_n_length(data, num_days, function='max'):
    data = list(data)
    cur_sum = sum(data[0:num_days])
    for i in range(len(data)-num_days + 1):
        i_sum = sum(data[i:i+num_days])
        if i_sum > cur_sum and function == 'max':
            cur_sum = i_sum
        elif i_sum < cur_sum and function == 'min':
            cur_sum = i_sum
    return cur_sum

def cons_sum(data, num_days, function='max'):
    return xr.apply_ufunc(find_sum_n_length,
                          data,
                          input_core_dims=[['time']],
                          kwargs={
                              'num_days': num_days,
                              'function': function
                          },
                          dask='allowed',
                          vectorize=True)


def tabularOuputModelYear(grid, varname, shapefile, shapefileID,function = "mean"):
    res = {}

    ## Prepare Shapefile
    shapes = geo.read_file(shapefile)
    num_cols = len(shapes[shapefileID])

    ## Prepare Data
    grid = grid[varname]
    spl_varname = varname.split('_')
    print(spl_varname)
    cur_tf = spl_varname[0]  ## This will be year, month or season.
    grid = grid.rename({'lon':'x','lat':'y'})
    grid.rio.write_crs("epsg:4326", inplace=True)  ## ASK RYAN ABOUT THIS.

    models = grid['model'].values
    times = grid['year'].values
    print(models)
    print(times)

    ## Prepare Return Variable
    res = {}
    model_col = np.repeat(models,len(times))
    model_col.sort()
    res['model'] = model_col
    res['year'] = np.repeat(times, len(models))
    num_rows = len(models) * len(times)

    ## Create the Columns
    for index, row in shapes.iterrows():
        cur_row = shapes.iloc[index:index + 1]
        cur_col_name = cur_row[shapefileID].values[0]
        res[cur_col_name] = np.empty(shape=num_rows)  ## will be a random number but faster than 0's.
        grid.rio.write_crs("epsg:4326", inplace=True)  ## ASK RYAN ABOUT THIS.
        clipped = grid.rio.clip(cur_row.geometry.apply(mapping), cur_row.crs, drop=False,
                                all_touched=True)
        row_index = 0
        for model in grid['model'].values:
            for time in grid['year'].values:
                ## This is one row.
                ##TODO: Only works for years
                cur_grid = clipped.sel(year=str(time), model=model)

                value = helper_function_dist(cur_grid, function)
                res[cur_col_name][row_index] = value
                res['model'][row_index] = model
                res['year'][row_index] = str(time)[:10]
                row_index += 1

    return pd.DataFrame(res)

def tabularOutput(grid, listofvars, shapefile, shapefileID, function='mean', modelTime=False):
    """
    :param grid xarray DataArray of object with lat, lon (no time)
    :param listofvars list of strings variables to output
    :param shapefile: Area of Interest (polygons) (relative location to shapefile)
    :param shapefileID: column in shapefile to use as ID Assume just 1 row for each shapefileID
            NOTE: User can make a multipart polygon if needed.
    :param function =>> function to be used across the space. allowable options are:
                max, min, mean, sum
    :return: Pandas DataFrame with columns: shapefileID, [listofVars]
            The result is in m^3
    """
    ### Error Checking

    if (modelTime):
        # Return the first element of the list.
        return(tabularOuputModelYear(grid,listofvars[0], shapefile, shapefileID, function))

    avail_vars = list(grid.data_vars.keys())
    if len(listofvars) == 0:
        warnings.warn("List of vars is empty. Please provide a variable")
        return
    try:
        for var in listofvars:
            if var not in avail_vars:
                messages = "ERROR: One of your vars is not in the grid ({})".format(var)
                warnings.warn(messages)
                return

    except:
        warnings.warn("An error checking the variables.")
        return

    ## First will need to create a coordinate for pixel area.
    cellExtentinLatitude = abs(grid['lat'].isel(lat=2).values - grid['lat'].isel(lat=1).values)
    base_mat = np.empty(shape=(130, 100))
    height_mat = np.empty(shape=(130, 100))
    x_int = 0
    for x_value in grid.lon:
        y_int=0
        for y_value in grid.lat:
            base_mat[x_int, y_int] = abs(np.cos(y_value)*11110 * cellExtentinLatitude)
            height_mat[x_int, y_int] = cellExtentinLatitude *111110
            y_int +=1
        x_int += 1
    area_array = base_mat * height_mat
    grid['area'] = (('lon','lat'), area_array)
    grid = grid[listofvars + ['area']] ## Only select subset for now.
    grid = grid.rename({'lon':'x','lat':'y'})
    grid.rio.write_crs("epsg:4326", inplace=True) ## ASK RYAN ABOUT THIS.

    ### Second run the value for each shape file.
    res = {}
    try:
        shapes = geo.read_file(shapefile)

        ### Initialize the Resulting Data Frame
        res[shapefileID] = shapes[shapefileID]
        num_shapes = len(shapes[shapefileID])
        res['computed_area'] = np.empty(shape=num_shapes)
        for index, row in shapes.iterrows():
            cur_row = shapes.iloc[index:index+1]
            clipped = grid.rio.clip(cur_row.geometry.apply(mapping), cur_row.crs, drop=False, all_touched=True)
            area = helper_function_dist(clipped.area, 'sum')
            res['computed_area'][index] = area
            for var in listofvars:
                spl_varname = var.split('_')
                cur_tf = spl_varname[0] ## This will be year, month or season.
                if cur_tf == 'year':
                    if index == 0:
                        # If we've never encountered this before, lets setup the variables.
                        res[var] = np.empty(shape=num_shapes)  ## will be a random number but faster than 0's.
                    value = helper_function_dist(clipped[var], function)
                    res[var][index] = value
                elif cur_tf == 'month':
                    for cur_month in list(grid['month'].values):
                        sub_var = var + '_' + str(cur_month)
                        if index == 0:
                            res[sub_var] = np.empty(shape=num_shapes) ## initialize
                        value = helper_function_dist(clipped.sel(month=cur_month)[var], function)
                        res[sub_var][index] = value
                elif cur_tf == 'season':
                    for cur_season in list(grid['season'].values):
                        sub_var = var + '_' + str(cur_season)
                        if index == 0:
                            res[sub_var] = np.empty(shape=num_shapes)
                        value = helper_function_dist(clipped.sel(season=cur_season)[var], function)
                        res[sub_var][index] = value

    except:
        warnings.warn("Something went wrong with the shape file")
        print("Unexpected error:", sys.exc_info()[0])
        return

    return pd.DataFrame(res)

def helper_function_dist(grid, function):
    """
    TODO: This HASSSS to be implemented elsewhere somewhere. Find it and use that instead.
    :param grid: a basic grid input
    :param function: what function to run on it
    :return: a float
    """

    if function == 'mean':
        return float(grid.mean())
    elif function == "max":
        return float(grid.max())
    elif function == "min":
        return float(grid.min())
    elif function == "sum":
        return float(grid.sum())

    warnings.warn("Wrong function provided")