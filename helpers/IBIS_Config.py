import numpy as np
import pandas as pd
from datetime import datetime



all_models = ['bcc-csm1-1', 'CCSM4', 'CNRM-CM5', 'GFDL-ESM2M', 'IPSL-CM5A-LR', 'MIROC5', 'MRI-CGCM3']
timeperiod_dict = {"HIST": "1980-1999", "MID": "2040-2059", "END": "2080-2099"}
RCPs = ['HISTORIC', 'RCP4.5', 'RCP8.5']
## NOTE: You pick which metrics you want. TODO: Fix to be able to have both corn variables in. THis is currnetly a bug.
IBIS_vars = ['aet', 'drainage', 'srunoff', 'trunoff'] # 'cropyld_Mgha', 'no3leach',
time_freqs = {"year": "1YS", "month": "1MS", "season": "1QS-DEC"}
## This is used because we never use them and then it speeds it up. Basically vars to skip on import.
vars_to_drop = ['relh','wspd','rads']

units = {
    "tmin": "celcius",
    "tmax": "celcius",
    "prec": "mm/day",
}


def getStatStr(statistic):
    """
    All of these must start with the time_freq. This is used in other locations to make this happen.
    :param statistic:
    :return:
    """
    ## TODO Somehow use this function, it might require a larger refactor so each statistic function doesn't need all the inputs
    if statistic['stat_type'] == 'average':
        var = 'prec'
        if "var" in statistic:
            var = statistic['var']
        return '{}_{}_{}'.format(statistic['time_freq'], statistic['function'], var)
    elif statistic['stat_type'] == 'threshold':
        text = 'above' if statistic['above'] else 'below'
        return "{}_{}_{}_{}".format(statistic['time_freq'], text, str(statistic['threshold']), statistic['var'])
    elif statistic['stat_type'] == 'consecutive':
        text = 'above' if statistic['above'] else 'below'
        if statistic['start_date']:
            return '{}_{}_max_con_sd_{}_{}'.format(statistic['time_freq'], statistic['var'], text, statistic['threshold'])
        return '{}_{}_max_con_{}_{}'.format(statistic['time_freq'], statistic['var'], text, statistic['threshold'])
    elif statistic['stat_type'] == 'consecutive_sum':
        return '{}_{}_consum_{}_{}_days'.format(
            statistic['time_freq'],
            statistic['function'],
            statistic['var'],
            statistic['numDays'])
    elif statistic['stat_type'] == 'subset':
        ## 'year_mean_mask_05010901_prec'
        new_stat = statistic['statistic'].copy()
        name = "mask" + statistic['start_date'].replace("/","") + statistic['end_date'].replace("/","") + '_' + new_stat['var']
        new_stat['var'] = name
        return getStatStr(new_stat)
    return ""

def convertTime(ibisData, startdatestr):
    start_date = np.datetime64(startdatestr)
    time_vals = ibisData['time'].values
    if len(time_vals) == 240:
        time_dim = 'month'
    elif len(time_vals) == 20:
        time_dim = 'year'
    else:
        return
    dates_list = [fuzzyAddTime(start_date, x - 1, time_dim) for x in list(ibisData['time'].values)]
    ret = ibisData.rename({'time': 'time_old'})
    ret.coords['time'] = (('time_old'), dates_list)
    ret = ret.swap_dims({'time_old': 'time'})
    ret = ret.rename({'latitude':'lat', 'longitude':'lon'})
    return ret.drop('time_old')

def getStartYear(time_period):
    return int(timeperiod_dict[time_period][:4])

def betweenDates(data, start_date, end_date):
    '''

    :param data: an array of datetimes
    :param start_date: Note in the format of: '05/01'
    :param end_date:  Note in the format of: '05/01'
    :return: an array of logicals (true/ false) if each date was betewen supplied dates
    '''
    ## ERROR CHECK:
    if (start_date > end_date):
        print("Start date is after end date")
        return data
    # TODO check type

    pdas_newformat = np.array([pd.to_datetime(x).strftime('%m/%d') for x in data])

    return (end_date >= pdas_newformat) & (pdas_newformat >= start_date)

