from classes.reportGenerator import ReportGenerator
from datetime import datetime
from helpers import xarrayHelpers as xrh
import os
import configparser
from pathlib import Path
import json

### Config
config = configparser.ConfigParser()
config.read('config.ini')
LCCMR = config['file_locations']['lccmr']
Climate_Data = config['file_locations']['stats']
output_loc = config['file_locations']['output']
template_tif = config['file_locations']['tif']

shp_file = config['tabular']['shp_file']
shp_name = config['tabular']['shp_col']
os.makedirs(output_loc, exist_ok=True)


#### Net Ensemble Run

model_suite = ['bcc-csm1-1', 'CCSM4', 'CNRM-CM5', 'GFDL-ESM2M', 'IPSL-CM5A-LR', 'MRI-CGCM3', 'MIROC5']  # Full Models

### RUN CONFIG
generateStats = False
generateMaps = False
generateTabular = True
generateTabularFullData = False

# The full list of statistics used in the report.
with open('statistics.json') as f:
    statistics = json.load(f)

report_full = ReportGenerator(
    LCCMR,
    Climate_Data,
    model_suite
)

if (generateStats):
    # This will check to make sure all of the stats are generated for each model/ scenario that is needed.
    report_full.generateStatistics(statistics)

# This will get all of the ensembles, may take a minute to load into memory.

print(report_full)
## TODO: Make a report print statement.

hist = report_full.getEnsemble("HIST")

print(hist)


#
# # This will generate the old report tiffs.
# report_full.generateOldReportTiffs(output_loc)
if generateMaps:
    scenarios = ['MID', 'END4.5', 'END8.5']
    vars = ['year_mask05010930_prec_max_con_below_0.2541', 'year_sum_mask04200510_prec']
    print("OUTPUT LOCATION IS: ")
    print(output_loc)
    for scen in scenarios:
        reg = xrh.returnOldMasked(report_full.diffEnsembles('HIST',scen,  precent=False))
        perc = xrh.returnOldMasked(report_full.diffEnsembles('HIST',scen,  precent=True))
        for var in vars:
            xrh.array2geotiff(
                reg[var],
                template_tif,
                (Path(output_loc) / '{}_{}_abs.tif'.format(scen, var)),
            )
            xrh.array2geotiff(
                perc[var],
                template_tif,
            (Path(output_loc) / '{}_{}_perc.tif'.format(scen, var)),
            )
            print("Successful output for: " + scen)

## Example for Full:

if generateTabularFullData:
    var = ['year_mean_prec']
    scenarios = ['HIST']
    for scen in scenarios:
        grid = xrh.returnOldMasked(report_full.getDataPoints(scen))
        ret = xrh.tabularOutput(
            grid,
            var,
            shp_file,
            shp_name,
            modelTime = True
        )
        ret.to_csv(Path(output_loc) / 'TabularOutput' /'datapoints_' + scen + '.csv')


if generateTabular:
    listofvars = ['month_mean_prec','year_mean_prec','year_max_consum_prec_5_days','year_max_prec']
    scenarios = ['HIST']
    ret = {}
    for scen in scenarios:
        grid = xrh.returnOldMasked(report_full.getEnsemble(scen))
        ret = xrh.tabularOutput(
            grid,
            listofvars,
            shp_file,
            shp_name
        )
        ret.to_csv(Path(output_loc) / 'Tabular Output' / 'fix_{}_{}_mm.csv'.format(scen, len(model_suite)))
        ## NO END 4.5??