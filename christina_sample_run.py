from reportGenerator import ReportGenerator
from datetime import datetime
import xarrayHelpers as xrh
import os
import numpy as np
from pathlib import Path


# Christina
# os.getcwd()
os.chdir("C:/Users/clocke/Desktop/climatedata/climatedata/")
LCCMR = Path("G:/My Drive/LCCMR_IBIS/LCCMR_IBIS/")
# Climate_Data = Path("G:/Shared drives/Climate Data/")
Climate_Data = 'G:/Shared drives/Climate Data/'
output_loc = Path("C:/Users/clocke/Desktop/climatedata_output/")
shp = Path("E:/KeelerLab/shp_bdry_counties/County_Boundaries_in_Minnesota_WGS.shp")
# shp = 'C:/Users/clocke/Desktop/climatedata/climatedata/tests/test_data/candidate_cities_wgs1984/candidate_cities_wgs1984.shp'

#### Net Ensemble Run

model_suite = ['bcc-csm1-1', 'CCSM4', 'CNRM-CM5', 'GFDL-ESM2M', 'IPSL-CM5A-LR', 'MRI-CGCM3', 'MIROC5']  # Full Models
# model_suite = ['bcc-csm1-1', 'CCSM4', 'CNRM-CM5', 'GFDL-ESM2M', 'IPSL-CM5A-LR'] # Report Models
# model_suite = ['bcc-csm1-1', 'CCSM4', 'CNRM-CM5', 'GFDL-ESM2M'] # IBIS Report Models
# model_suite = ['bcc-csm1-1', 'CCSM4']  # 'CNRM-CM5', 'GFDL-ESM2M', 'IPSL-CM5A-LR'] # Small Test
# output_loc += '_' + str(len(model_suite)) + 'models/'
os.makedirs(output_loc, exist_ok=True)

### RUN CONFIG
generateStats = False
generateMaps = True
generateTabular = False
generateTabularFullData = False

# This is the full list of statistics that is used in the report.
statistics = [
    ## TODO: Check that if you add this twice it doesn't do the average twice?
    {
        "stat_type": "average",
        "time_freq": "year",
        "function": "mean",
        "var": "prec",
    }, {
        "stat_type": "average",
        "time_freq": "year",
        "function": "max",
        "var": "prec",
    }, {
        "stat_type": "threshold",
        'var': 'tmax',
        "threshold": 35,  # 95 celcius
        "above": True,
        "time_freq": "year",
    }, {
        "stat_type": "average",
        "time_freq": "month",
        "function": "mean"
    }, {
        "stat_type": "threshold",
        'var': 'tmin',
        "threshold": 0,  # below 0
        "above": False,
        "time_freq": "year",
    }, {
        "stat_type": "average",
        "time_freq": "season",
        "function": "mean"
    }, {
        'stat_type': 'consecutive',
        'var': 'prec',
        'threshold': 0.2541, ## 0.01 inches (this is in mm)
        'above': False,
        'time_freq': 'year',
        'start_date': False
    }, {
        'stat_type': 'consecutive_sum',
        'var': 'prec',
        'numDays': 5,
        'function': 'max',
        'time_freq': 'year'
    },  {
        "stat_type": "threshold",
        'var': 'prec',
        "threshold": 104.14,  # 4.1 inches
        "above": True,
        "time_freq": "year",
    }, {
        "stat_type": "threshold",
        'var': 'prec',
        "threshold": 149.86,  # 5.9 inches
        "above": True,
        "time_freq": "year",
    },    {
        "stat_type": "subset",
        "start_date": "05/01",
        "end_date": "09/30",
        "statistic": {
            'stat_type': 'consecutive',
            'var': 'prec',
            'threshold': 0.2541, ## 0.01 inches (this is in mm)
            'above': False,
            'time_freq': 'year',
            'start_date': False
        },
    },  {
        "stat_type": "subset",
        "start_date": "06/01",
        "end_date": "08/31",
        "statistic":
            {
                "stat_type": "average",
                "time_freq": "year",
                "function": "sum",
                "var": "prec",
            },
    },
]

report_full = ReportGenerator(
    LCCMR,
    Climate_Data,
    model_suite
)

if (generateStats):
    # This will check to make sure all of the stats are generated for each model/ scenario that is needed.
    report_full.generateStatistics(statistics)

# This will get all of the ensembles, may take a minute to load into memory.

# print all variables
hist = report_full.getEnsemble('HIST')
print(hist)
hist['season'] # 'DJF' 'JJA' 'MAM' 'SON'

#
# # This will generate the old report tiffs.
# report_full.generateOldReportTiffs(output_loc)
if generateMaps:
    scenarios = ['END4.5']
    vars = ['year_sum_mask06010831_prec']
    print("OUTPUT LOCATION IS: ")
    print(output_loc)
    for scen in scenarios:
        reg = xrh.returnOldMasked(report_full.diffEnsembles('HIST',scen,  precent=False))
        perc = xrh.returnOldMasked(report_full.diffEnsembles('HIST',scen,  precent=True))
        for var in vars:
            xrh.array2geotiff(
                reg[var],
                'template.tif',
                '{}/{}_{}_abs.tif'.format(output_loc,scen, var),
            )
            xrh.array2geotiff(
                perc[var],
                'template.tif',
                '{}/{}_{}_perc.tif'.format(output_loc,scen, var),
            )
            print("Successful output for: " + scen)

## Example for Full:

if generateTabularFullData:
    var = ['year_mean_prec']
    scenarios = ['HIST']
    for scen in scenarios:
        output_file = Path(output_loc,'Tabular Output','datapoints_' + scen + '_' + str(len(model_suite)) + '.csv')
        grid = xrh.returnOldMasked(report_full.getDataPoints(scen))
        ret = xrh.tabularOutput(
            grid,
            var,
            shp,
            'COUNTY_NAM',
            modelTime=True
        )
        ret.to_csv(output_file)


if generateTabular:
    listofvars = ['season_mean_prec']
    scenarios = ['HIST','MID', 'END4.5', 'END8.5']
    ret = {}
    for scen in scenarios:
        output_file = Path(output_loc,'Tabular Output','fix_' + scen + '_' + str(len(model_suite)) + '_mm.csv')
        grid = xrh.returnOldMasked(report_full.getEnsemble(scen))
        ret = xrh.tabularOutput(
            grid,
            listofvars,
            shp,
            'COUNTY_NAM'
        )
        ret.to_csv(output_file)  
        ## NO END 4.5??