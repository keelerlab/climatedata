## THis is a file for Ryan to determine how to look at it.

# To look at the entire ensembles

# Ryan
LCCMR = r'C:\Users\rrnoe\Work\Climate_Change\google_drive\LCCMR_IBIS\\'
## NOTE You should download or put the netcdfs that i already generated so you don't have to generat them otherwise it might take awhile. Regardless
Climate_Data = r'C:\Users\rrnoe\Work\Climate_Change\google_drive\LCCMR_IBIS\\'

LCCMR = '/Volumes/GoogleDrive/My Drive/LCCMR_IBIS/'
Climate_Data = '/Volumes/GoogleDrive/Shared drives/Climate Data/'


#### To get the MIROC data
from classes.modelStatistics import ModelStatistics

MIROC = ModelStatistics(
            LCCMR,
            'MIROC5',
            'HIST',
            'HISTORIC',
            Climate_Data
        )

# Check to make sure that the days variable has been calcualted. Note that hte days variable is:
# 'year_prec_max_con_below_0.2541'
MIROC.getAvailableStats()

## You can change this but if you want to create it from scratch you can do that here.
if 'year_prec_max_con_below_0.2541' not in MIROC.getAvailableStats():
    ## Adds a variable for the length of the longest
    statistic = {
        'stat_type': 'consecutive',
        'var': 'prec',
        'threshold': 0.2541,
        'above': False,
        'time_freq': 'year',
        'start_date': False
    }
    MIROC.addStatistic(statistic)
    ## Adds a variable for when the drought starts (the longest one)
    statistic = {
        'stat_type': 'consecutive',
        'var': 'prec',
        'threshold': 0.2541,
        'above': False,
        'time_freq': 'year',
        'start_date': True
    }
    MIROC.addStatistic(statistic)

## Get that data from the netcdf
res = MIROC.getStatistics()['year_prec_max_con_below_0.2541']
res_sd = MIROC.getStatistics()['year_prec_max_con_sd_below_0.2541']
## This is an xarray dataset now that has all of the years, and the values for consecutive dry days for  MIROC.

## To create a GEOtiff at the values for any particular year, do:
from helpers import xarrayHelpers as xrh

xrh.array2geotiff(
    res.sel(year='1988-01-01').values,
    'template.tif',
    'test_output_tifs/dry2days1988.tif'
)

xrh.array2geotiff(
    xrh.returnOldMasked(res).sel(year='1988-01-01').values,
    'template.tif',
    'test_output_tifs/dry2days1988_mask.tif'
)

## To determine when in 1988 the dry days period was do:
MIROC.setData()
daily_data = MIROC.data['prec'].sel(time=slice('1988-01-01', '1988-12-31'))
## You'll have to pick a location to do it for
start_index = xrh.find_longest_consecutive(daily_data.isel(lon=50, lat=50).values,
                                           0.2541,
                                           False,
                                           True)

## The resulting start date of that particular
MIROC.data['prec'].sel(time=slice('1988-01-01','1988-12-31')).isel(time=start_index)


GFDL = ModelStatistics(
            LCCMR,
            'GFDL-ESM2M',
            'END',
            'RCP8.5',
            Climate_Data
        )