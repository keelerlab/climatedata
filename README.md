This repository is to process climate data that was provided by Tracy Twine. 

## Requirements
This repository was built in Python 3.y

The following python packages need to be installed piror to use: 
* array
* dask
* toolz - For some reason this isn't automatically installed. 
* rasterio 

The following folders you will need access to: 
* LCCMR_IBIS

The folder structure has been agreed upon and is documented here: ___ . (TODO FIND THIS)

## Setup
In order to use the repository, you must first create a `config.ini` file that is in the main folder. The file should look
like this
```python
[file_locations]
lccmr = /Volumes/GoogleDrive/My Drive/LCCMR_IBIS
stats = /Volumes/GoogleDrive/Shared drives/Climate Data/Climate Change Projections/One Model
output = /Volumes/GoogleDrive/Shared drives/Climate Data/Climate Change Projections/
```

`lccmr` -- Where the LCCMR data is located. This is only needed if you would like to *generate statistics*.
   
`stats` -- Where the `statistics.nc` files are located. This is needed if you want to look at modeled statistics  or
ensembled statistics. 

`output` -- Output location where files will be put. This is not required unless using a copy of `sample_run.py`. 

Note all of these paths should be relative to your machine. 

## Example
The file `sample_run.py` is an example file for how you could run climate code. This file is not meant to be exactly
how you might use the code. Feel free to create your own python code that uses it. 


## Class Structure
NOTE: Names are to be desired. I'm not good at naming. Sorry!

`ModelStatistics` focuses on one model. This class will mainly create and output statistics at different timescales
for one particular model. For example, precip may be re-output from a daily scale to a monthly scale or a seasonal
 or annual scale depending on what you would want. This is output in the output location under 
 `/Climate Change Projections/One Model`. 
 
 `NetcdfEnsemble` 
 
 ## Time Frequency Options
 Currently there are 3 time frequencies that can be aggregated to: 
 
 - Month
 - Season -- Currently season is custom defined to end with Aug. This is June-July-Aug are aggregated together.
 - Year
 
 These are defined in `IBIS_Config.py`. A custom frequency can be added using the pandas resample syntax. More information can be found: 
 at either of these sites:
 
 - https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#offset-aliases
- https://towardsdatascience.com/using-the-pandas-resample-function-a231144194c4
 
 ## Statistic Dictionary
 In this there are a few different types of statistics that can be generated. To create a new statistic, a statistic 
 dictionary will be added to the file. This is useful so that you can have a list  of dictionary values, iterate through
 them, and then add all of the statistics. This section will detail out the different statistic types that can be ran
 as well as their outputs. 
 
 For each type listed below, each value that is expected will be listed. 
 
 The available statistic types (`stat_type`) are: 
 1. `average` run with `ModelStatistics.computeStatistics` will return the function on all variables for  the given time
 period. It requires the following values: 
     
     - `time_freq` -- Default: `month`
     - `function` -- Default: `mean`
     
NOTE: `average` is slightly misleading as you can apply other functions (`max`, `min` etc)
 
 A sample statistic dictionary set up looks like this: 
 ```python
statistic = {
    "stat_type": "average",
    "time_freq": "month",
    "function": "mean"
}
```

2. `threshold` run with `ModelStatistics.computeThresholdStatistics` will return the number of days above or 
below a particular threshold for a given timeperiod and variable. It requires the following values:

    - `threshold` -- Value that is the break point
    - `above` -- Boolean stating if we're looking above or below. Above = True -> data > threshold. 
    - `var` -- Variable
    - `time_freq` -- Default: `month`
    
A sample statistic dictionary set up looks like this: 
 ```python
statistic = {
    "stat_type": "threshold",
    "threshold": 0.2541, 
    "above": False,
    "time_freq": "month",
}
```

3. `maxSequence` run with `ModelStatistics.computeMaxSequence` will return the maximum sum for a particular number of days.
It requires the following values: 

    - `numDays` -- Number of days to sum
    - `var` -- Variable, Default: `prec`
    - `time_freq` -- Default: `month`
    
4. `computeConsecutive` run with `ModelStatistics.computeConsecutive` will return the function for the number of days above
or below a threshold. For this reason it requires all of the same values as `threshold` with one added of `function` to determine
how to combine them all back together. Aka for the given time period do you want the average number of consecutive days above
or below the thershold, the max, etc. 

    - `threshold` -- Value that is the break point
    - `above` -- Boolean stating if we're looking above or below. Above = True -> data > threshold. 
    - `var` -- Variable
    - `time_freq` -- Default: `month`
    - `function` -- Default: `mean`


5. `subset` This exists if you want to run on a subset of a year does not divide easily over the resample period
You enter a start and end date and the year will be masked and then run the underlying
statistic that is underneath. One note is that the underlying statistic should always
be annual as it will mean that it is going to be 

    - `start_date` -- The start date
    - `end_date ` -- The end date
    - `name` -- to be used for the naming
    - `statistic` -- an entire stat dictionary underneath