from classes.NetcdfEnsemble import NetcdfEnsemble
from classes.modelStatistics import ModelStatistics
import os
from helpers import IBIS_Config as ib, xarrayHelpers as xrh
import numpy as np


class ReportGenerator:

    def __init__(self, IBIS_location, output_location, models):
        """
        This initiates the set of model runs that you would like to run.
        :param output: Location where the outputs are Should end in Climate Data/ '/Volumes/GoogleDrive/Shared drives/Climate Data/'
        :param models: List of models to go through.
        """
        self.location = IBIS_location
        self.output = output_location
        self.models = models
        self.scenarios = {
            'HIST': dict(rcp='HISTORIC', timeframe='HIST'),
            'MID': dict(rcp='RCP4.5', timeframe='MID'),
            'END4.5': dict(rcp='RCP4.5', timeframe='END'),
            'END8.5': dict(rcp='RCP8.5', timeframe='END')
        }

        self.setScenarios()

    def __repr__(self):
        return "ReportGenerator() \n" + str(self.models)

    def __str__(self):
        return "member of Test"

    def generateStatistics(self, statistics):
        """
        This function is made to generate for the list of statistics that is given
        :param statistics: a list of dictionary statistic objects.
        :return: N/A This will output those statistics to the file that is needed.
        """
        for scenario in self.scenarios:
            print("Generating for: " + scenario)
            sc_info = self.scenarios[scenario]
            for model in self.models:
                print("For Model: " + model)
                model = ModelStatistics(
                    self.location,
                    model,
                    sc_info['timeframe'],
                    sc_info['rcp'],
                    self.output
                )
                for statistic in statistics:
                    print("Adding Statistic: " + ib.getStatStr(statistic))
                    model.addStatistic(statistic, overwrite=False)

    def setScenarios(self):
        for scenario in self.scenarios:
            print("Getting for scenario: " + str(scenario))
            sc_info = self.scenarios[scenario]
            net_ensemble = NetcdfEnsemble(
                self.location,
                self.output,
                self.models,
                sc_info['timeframe'],
                sc_info['rcp'],
            )
            sc_info['ensemble'] = net_ensemble
            self.scenarios[scenario] = sc_info

    def getEnsemble(self, scenario, function='mean', time_freq='month'):
        return self.scenarios[scenario]['ensemble'].ensemble(function=function, time_freq=time_freq)

    def getDataPoints(self, scenario):
        return self.scenarios[scenario]['ensemble'].getMergedModels()

    def diffEnsembles(self, scenario_1, scenario_2, precent=False):
        ens_2 = self.getEnsemble(scenario_2)
        ens_1 = self.getEnsemble(scenario_1)
        if precent:
            return (ens_2 - ens_1) / ens_1
        return ens_2 - ens_1

    def generateOldReportTiffs(self, filelocation):
        """ Outputs the report tifs for old in file location. """

        if not os.path.exists(filelocation):
            os.mkdir(filelocation)

        ## % Change in Annual Precip
        self.createDiffGeotiffs('year_mean_prec', 1, filelocation)

        ## % Change in May Precip
        self.createDiffGeotiffs('month_mean_prec', 5, filelocation)

        ## % Change in Aug Precip
        self.createDiffGeotiffs('month_mean_prec', 8, filelocation)

        ## % Change in Annual Average 5 Day Maximum
        self.createDiffGeotiffs('year_max_consum_prec_5_days',1,filelocation, percent=True)

        ## % Change in Average Dry Spell
        self.createDiffGeotiffs('year_prec_max_con_below_0.2541', 1, filelocation, percent=True)

        ## Additioanl Days Over 95 Farenheit
        self.createDiffGeotiffs('year_above_35_tmax', 1, filelocation, percent=False)

        ## Frost ?? What did we decide for this one?
        self.createDiffGeotiffs('year_below_0_tmin', 1, filelocation)

    def createDiffGeotiffs(self, var,filelocation, month=-1, percent=True):

        # diff mid
        diff_mid = xrh.returnOldMasked(self.diffEnsembles('HIST','MID',  precent=percent))
        diff_end4 = xrh.returnOldMasked(self.diffEnsembles('HIST','END4.5',  precent=percent))
        diff_end8 = xrh.returnOldMasked(self.diffEnsembles('HIST', 'END8.5', precent=percent))
        if month != -1:
            xrh.array2geotiff(
                diff_mid[var].sel(month=month).values,
                'template.tif',
                "{}{}_{}_MID.tif".format(filelocation, var, month)
            )
            # diff end 4.5
            xrh.array2geotiff(
                diff_end4[var].sel(month=month).values,
                'template.tif',
                "{}{}_{}_END45.tif".format(filelocation, var, month)
            )
            # diff end 8.5
            xrh.array2geotiff(
                diff_end8[var].sel(month=month).values,
                'template.tif',
                "{}{}_{}_END85.tif".format(filelocation, var, month)
            )
        else:
            xrh.array2geotiff(
                diff_mid[var].values,
                'template.tif',
                "{}{}_{}_MID.tif".format(filelocation, var, month)
            )
            # diff end 4.5
            xrh.array2geotiff(
                diff_end4[var].values,
                'template.tif',
                "{}{}_{}_END45.tif".format(filelocation, var, month)
            )
            # diff end 8.5
            xrh.array2geotiff(
                diff_end8[var].values,
                'template.tif',
                "{}{}_{}_END85.tif".format(filelocation, var, month)
            )

    def diffOfMeans(self, scen1, scen2):

        ## Get first ensemble
        ## TODO Select properly.
        mu_1 = self.getEnsemble(scen1, function = 'mean')
        var_1 = self.getEnsemble(scen1, function = 'var')
        cnt_1 = self.getEnsemble(scen1, function = 'count')


        ## Get second ensemble
        mu_2 = self.getEnsemble(scen2, function = 'mean')
        var_2 = self.getEnsemble(scen2, function = 'var')
        cnt_2 = self.getEnsemble(scen2, function = 'count')

        STE = np.sqrt((var_1 / cnt_1) + (var_2 / cnt_2))
        EST = mu_1 - mu_2

        z_score = EST / STE

        return z_score

