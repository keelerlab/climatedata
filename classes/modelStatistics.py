import xarray as xr
import os
import numpy as np
np.seterr(divide='ignore', invalid='ignore')
from pathlib import Path
from helpers import IBIS_Config as ib, xarrayHelpers as xhr
import glob
import warnings
warnings.filterwarnings("ignore", message="numpy.dtype size changed")
warnings.filterwarnings("ignore", message="numpy.ufunc size changed")


class ModelStatistics:
    """ This class handles a single model and will create a netcdf of certain statistics that would be needed for it."""
    # TODO: For right now all of these only take in WRF inputs but may eventually want to do more AgroIBIS outputs

    all_time_freq = ib.time_freqs

    # http://xarray.pydata.org/en/stable/time-series.html

    ### Setters
    def __init__(self, location, model, timeperiod, rcp, output, add_cwd = False):
        """
        Sets up the instance to be able to gather the information for a particular model.
        :param location: This is where the ensembles are. Assumed folder structure, should end with "LCCMR_IBIS/"
        :param model: This is a particular model: Note: needs to be in the list all_models
        :param timeperiod: HIST, MID, END are the options
        :param rcp: HISTORIC RCP4.5 RCP8.5
        """
        self.location = Path(location)
        assert model in ib.all_models
        self.model = model
        self.timeperiod = timeperiod
        self.rcp = rcp
        if add_cwd:
            self.output = Path(os.getcwd()) / Path(output)
        else:
            self.output = Path(output)

    def setData(self):
        """
        Gets the data associated with the model. TODO: Make this differntiate between WRF and AgroIbis.
        Sets the data variable.
        :return:
        """
        filePaths = []
        template_filename = "IBISinput_{}.nc"
        for year in self.getYearRange():
            filePaths.append(self.getFilePath() / template_filename.format(year))

        self.data = xr.open_mfdataset(
            filePaths,
            parallel=True,
            concat_dim="time",
            combine='by_coords').squeeze('lev')

        if self.data.sizes['lat'] == 105:
            print("Imported with mismatching latitudes. Re-importing.")
            year_datasets = []
            for file in filePaths:
                year_datasets.append(xr.open_dataset(file))
            year_datasets = xhr.fixLat(year_datasets)
            self.data = xr.concat(year_datasets, 'time').squeeze('lev')

        self.data = xhr.returnOldMasked(self.data)

    def assignUnits(self):
        for var in list(self.data.data_vars.keys()):
            self.data[var] = self.data[var].assign_attrs({'units': ib.units[var]})

    def getIBISData(self):
        file_path = self.getFilePath(type='IBIS')
        all_data_sets = []
        for var in ib.IBIS_vars:
            try:
                file_name = glob.glob(file_path + var + '*.nc')[0]
                data = xr.open_dataset(file_name, chunks={'time': 20, 'latitude': 100, 'longitude': 130})
                start_date = str(ib.getStartYear(self.timeperiod)) + '-01-01'
                data = ib.convertTime(data, start_date)
                all_data_sets.append(data)
            except:
                print("IBIS variable: " + var + " could not be found")
        return xr.merge(all_data_sets)

    def combineAndSave(self, statistics):
        """
        This function combines a set # of statistics and then saves them.
        :param statistics: list of different DataArray or DataSets ?? to be merged
        :param file_name: filename to save it as
        :return:
        """
        res = xr.merge(statistics)
        file_path = self.getFilePath('one')
        if not os.path.exists(file_path):
            os.makedirs(file_path)
        ## TODO Add to the attribute, the model when creating this. Mimick Terin's?

        res.close()
        res.to_netcdf(file_path /'statistics.nc', mode='w', format="NETCDF4", engine="netcdf4")

    ### Statistics
    def addStatistic(self, statistic, overwrite=True):
        """
        Add a new statistic to an existing packaged netcdf,
        :param statistic: Dictionary with keys: stat_type (threshold or average), above, var, timeext, time_freq, function
        :return:
        """
        file_path = self.getFilePath("one") / 'statistics.nc'
        if not overwrite:
            ## If we're not supposed to overwrite, first check if that already exists.
            if os.path.exists(file_path):
                data_vars = list(xr.open_dataset(file_path).data_vars.keys())
                if ib.getStatStr(statistic) in data_vars:
                    return

        ## Check if data exists if, not grab the wrf data.
        statistics = []
        if os.path.exists(file_path):
            prev_data = xr.open_dataset(file_path)
            statistics.append(prev_data.copy(deep=True))
            prev_data.close()
        if 'data' not in self.__dict__:
            self.setData()

        statistics.append(self.helper_addStatistic(statistic))

        self.combineAndSave(statistics)

    def helper_addStatistic(self, statistic):
        if statistic['stat_type'] == 'average':
            return(self.computeStatistics(
                self.data,
                statistic['time_freq'],
                statistic['function']))
        elif statistic['stat_type'] == 'threshold':
            return(self.computeThresholdStatistics(
                statistic['threshold'],
                statistic['above'],
                statistic['var'],
                statistic['time_freq']))
        elif statistic['stat_type'] == 'consecutive':
            return(self.computeConsecutive(
                statistic['threshold'],
                statistic['above'],
                statistic['var'],
                statistic['time_freq'],
                statistic['start_date']
            ))
        elif statistic['stat_type'] == 'consecutive_sum':
            return(self.computeMaxorMinSequence(
                statistic['numDays'],
                statistic['var'],
                statistic['time_freq'],
                statistic['function']
            ))
        elif statistic['stat_type'] == 'subset':
            ## filter the dataset.
            new_stat = statistic['statistic'].copy()
            ## Add a masked version.
            new_var = self.getDataMasked(statistic['start_date'], statistic['end_date'], new_stat['var'])
            new_stat['var'] = new_var
            new_stat['time_freq'] = 'year'
            return self.helper_addStatistic(new_stat)

        raise Exception('Wrong statistic provided')

    def computeStatistics(self, data, time_freq='month', function='mean'):
        """ Computes the 'function' statistic for all base values
        "This method applies the function to all of the variables in the dataset
        :param time_freq: must be in list from all_freq
        :param function: must be mean, std, max or min. #TODO figure out if you can .apply or something this.
        :return:
        """
        if time_freq not in self.all_time_freq.keys():
            print("Please input a time_freq that is in the list: " + str(self.all_time_freq))
            return False

        resample = data.resample(time=self.all_time_freq[time_freq], skipna=True, closed='left', label='left')
        if function == 'mean':
            data = resample.mean()
            ## We have to adjust per timeperiod here also.
            if time_freq == 'year':
                data['prec'] = data['prec'] * 365.25
            if time_freq == 'month':
                data['prec'] = data['prec'] * 30.42
            if time_freq == 'season':
                data['prec'] = data['prec'] * 30.42 * 3
        elif function == 'std':
            data = resample.std()
        elif function == 'max':
            data = resample.max()
        elif function == 'min':
            data = resample.min()
        elif function == 'sum':
            data = resample.sum()
        else:
            print("Please select from mean, std, max, min or program in a new function option!")
            return
        data_keys = list(data.data_vars.keys())
        data_values = ['{}_{}_{}'.format(time_freq, function, var) for var in data_keys]
        new_vars = dict(zip(data_keys, data_values))
        new_vars['time'] = time_freq
        return data.rename(new_vars)

    def computeThresholdStatistics(self, threshold, above, var, time_freq='month'):
        """ Computes the threshold statistic
        Returns the # of days above or below the threshold
        :param threshold: double of what the value is.
        :param above: Boolean above or below a threshold
        :param var: The variable in the climate data you want
        :param time_freq: These are all of the ones from all_freq
        :return:
        """
        if above:
            thresholdData = (self.data[var] > threshold)
            text = 'above'
        else:
            thresholdData = (self.data[var] < threshold)
            text = 'below'

        resample = thresholdData.resample(time=self.all_time_freq[time_freq], skipna=True).sum()
        resample = resample.rename({'time': time_freq})
        resample = resample.rename("{}_{}_{}_{}".format(time_freq, text, str(threshold), var))

        return resample

    def computeMaxorMinSequence(self, numDays, var, time_freq='month', function='max'):
        """For the specified number of days, computes teh maximum value within those days"""
        ## Compute maybe mix sequence in some number of days.
        array = self.data[var]
        ret = array.resample(time=self.all_time_freq[time_freq], skipna=True).map(xhr.cons_sum, args=(numDays, function))
        ret = ret.rename({'time': time_freq})
        ret = ret.rename('{}_{}_consum_{}_{}_days'.format(time_freq, function, var, numDays))
        return ret

    def computeConsecutive(self, threshold, above, var, time_freq='month', start_date=False):
        """ Computes the consecutive sequence above or below threshold for a timeperiod and var"""
        #TODO Make this do a max consecutive or an average consecutive.
        text = 'below'
        if above:
            text = 'above'
        array = self.data[var] # Select only the variable that we want.
        ret = array.resample(time=self.all_time_freq[time_freq], skipna=True).map(xhr.longest_consecutive, args=(threshold, above, start_date))
        ret = ret.rename({'time': time_freq})
        if start_date:
            ret = ret.rename('{}_{}_max_con_sd_{}_{}'.format(time_freq, var, text, threshold))
        else:
            ret = ret.rename('{}_{}_max_con_{}_{}'.format(time_freq, var, text, threshold))
        return ret

    ### All Getters

    def getAvailableStats(self):
        """
        List the available stats for this particular model.
        :return: list of available stats.
        """
        file_path = self.getFilePath("one") / 'statistics.nc'
        if os.path.exists(file_path):
            statistics = xr.open_dataset(file_path)
            return list(statistics.data_vars.keys())
        return []

    def getFilePath(self, type='input'):
        """
        This is progarmmed from this folder as of the date of change
        https://docs.google.com/document/d/1UgA6NYcqkiM4LEwXTTQqNb_GqhBIlbqX_vvQEL_M9Qg/edit
        :return: Filepath location for the
        """
        if type == 'input':
            return self.location / self.model / self.rcp / ib.timeperiod_dict[self.timeperiod] / 'WRF_IBISinput'

        elif type == 'one':
            return self.output / self.model / self.rcp / ib.timeperiod_dict[self.timeperiod]

        elif type == 'IBIS':
            return self.location / self.model / self.rcp / ib.timeperiod_dict[self.timeperiod]

    def getYearRange(self):
        yearRange = ib.timeperiod_dict[self.timeperiod]
        startYear = (int)(yearRange[0:4])
        endYear = (int)(yearRange[-4:]) + 1
        return list(range(startYear, endYear))

    def getStatistics(self):
        file_path = self.getFilePath("one") / 'statistics.nc'

        if os.path.exists(file_path):
            return xr.open_dataset(file_path)
        print("No Available Stats")

    def getDataMasked(self, start_date, end_date, var):
        ## check if already masekd.
        name = "mask" + start_date.replace("/","") + end_date.replace("/","") + '_' + var
        if name in list(self.data.data_vars.keys()):
            return name
        mask = ib.betweenDates(self.data['time'].data, start_date, end_date)
        new_data = self.data[var]
        new_data.coords['mask'] = (('time'), mask)
        mask_data = new_data.where(new_data.mask == True)
        self.data = self.data.assign({name: mask_data.drop_vars('mask')})
        return name



