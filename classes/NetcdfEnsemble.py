import xarray as xr
from classes.modelStatistics import ModelStatistics
import pandas as pd
import helpers.xarrayHelpers as xhr
import helpers.IBIS_Config as ib


class NetcdfEnsemble:


    def __init__(self, IBIS_location, output_location, models, timeperiod, RCP):
        """
        This initiates the set of model runs that you would like to run.
        :param output: Location where the outputs are Should end in Climate Data/ '/Volumes/GoogleDrive/Shared drives/Climate Data/'
        :param models: List of models to go through.
        """
        self.location = IBIS_location
        self.output = output_location
        assert len(models) != 0
        self.models = []
        self.RCP = RCP
        self.timeperiod = timeperiod
        for model in models:
            cur_model = ModelStatistics(
                IBIS_location,
                model,
                timeperiod,
                RCP,
                output_location
            )
            self.models.append(cur_model)

    ## When importing, print list of available stats that exist in all models.
    def getListofStatistics(self, debug=True):
        """
        Returns currently available list of statistics in the model suite. This function is useful for knowing what
        may be needed to be calculated or what you can currently ensemble.
        :return:
        """
        listoflists = []
        for model in self.models:
            cur_model = model.getAvailableStats()
            listoflists.append(cur_model)
        ret = list(set.intersection(*map(set, listoflists)))
        ret.sort()  ## Self asserting function

        return ret

    def addStatistic(self, statistic):
        """
        See Read Me for documentatation on the statistic dictionary
        :param statistic: Statistic Dictionary.
        :return:
        """
        stat_str = ib.getStatStr(statistic)
        for model in self.models:
            if stat_str not in model.getAvailableStats():
                model.addStatistic(statistic)

    def ensemble(self, function='mean', time_freq='month'):
        """ Combines all of the files and saves them to the file. """
        ## TODO Add ability that not all models would need to have had the same function in them.

        all_values = self.getMergedModels()
        dims = ['model']
        coords = list(all_values.coords)
        coords.remove('lev')
        coords.remove('lon')
        coords.remove('lat')
        for val in coords:
            dims.append(val)

        # TODO Write a test_output_tifs that will double check if a model outputs this won't be implemented.
        # Let's do each separately and then collapse again.
        # get list of seasons
        all_results = []
        var_list = list(all_values.data_vars.keys())
        for time_freq in ib.time_freqs.keys():
            # Select variables
            cur_vars = [idx for idx in var_list if idx[0:len(time_freq)] == time_freq]
            if len(cur_vars) == 0:
                continue
            # do a group on those
            if time_freq == 'year':
                dims = ['year','model']
                vals = all_values[cur_vars]

            else:
                dims = [time_freq, 'model']
                vals = all_values[cur_vars].groupby(time_freq + '.'+ time_freq)

            ## Function change
            if function == 'mean':
                cur_ret = vals.mean(dim=dims, skipna=False)
            elif function == 'var':
                cur_ret = vals.var(dim=dims, skipna=False)
            elif function == 'count':
                cur_ret = vals.count(dim=dims)

            # add to list
            all_results.append(cur_ret)


        ret = xr.merge(all_results)
        return ret

    def ensembleIbis(self, var, function='mean'):
        """
        Gets the IBIS outputs and ensembles them.
        :param var:
        :param function:
        :return:
        """
        model_list = []
        name_list = []
        for model in self.models:
            ## Go and get the IBIS output
            return
        return

    def getEnsStr(self):
        ret = "ens_{}_{}_{}".format(str(len(self.models)), self.RCP, self.timeperiod)
        return ret

    def getMergedModels(self):
        ens_stats = self.getListofStatistics(debug=False) # List of all possible statistics that exist across all models.
        model_list = []
        name_list = []
        coord_list = []

        for model in self.models:
            model_stats = model.getAvailableStats()
            ## Remove vars that are not in the entire ensemble
            model_datavars = [x for x in model_stats if x not in ens_stats]
            cur_model = model.getStatistics().drop_vars(model_datavars)
            coord_list.append(list(cur_model._coord_names))
            model_list.append(cur_model)
            name_list.append(model.model)

        new_coords = list(set.intersection(*map(set, coord_list)))
        new_model_list = []
        for model in model_list:
            coords_to_drop = [x for x in list(model._coord_names) if x not in new_coords]
            new_model_list.append(model.reset_index(coords_to_drop, drop=True))

        model_list = xhr.fixLat(new_model_list)
        ## TODO Add for one model.
        return xr.concat(model_list, pd.Index(name_list, name='model'))
