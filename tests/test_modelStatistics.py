from unittest import TestCase
from classes.modelStatistics import ModelStatistics
from helpers import IBIS_Config as ib
import os
import configparser
from pathlib import Path


class TestModelStatistics(TestCase):
    def setUp(self):
        config = configparser.ConfigParser()
        self.curpath = Path(__file__).parent.absolute()
        config.read( self.curpath / '../config.ini')
        self.location = config['file_locations']['lccmr']
        Climate_Data = '/tmp'
        ## TODO Clean up directory
        self.modelTest = ModelStatistics(
            self.location,
            'CCSM4',
            'HIST',
            'HISTORIC',
            Climate_Data
        )
        self.modelTestMid = ModelStatistics(
            self.location,
            'CCSM4',
            'MID',
            'RCP4.5',
            Climate_Data
        )
        self.modelTestEnd = ModelStatistics(
            self.location,
            'CCSM4',
            'END',
            'RCP8.5',
            Climate_Data
        )

class TestGetData(TestModelStatistics):
    def test_get(self):
        self.modelTest.setData()

class TestGetModel(TestModelStatistics):
    def test_get(self):
        self.assertEqual(
            self.modelTest.getFilePath(),
        Path(self.location) / 'CCSM4' / 'HISTORIC'/'1980-1999'/ 'WRF_IBISinput')

class TestGetYearRange(TestModelStatistics):
    def test_hist(self):
        self.assertEqual(
            self.modelTest.getYearRange(),
            [1980, 1981, 1982, 1983, 1984, 1985, 1986, 1987, 1988, 1989,
             1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999]
        )

    def test_mid(self):
        self.assertEqual(
            self.modelTestMid.getYearRange(),
            [2040, 2041, 2042, 2043, 2044, 2045, 2046, 2047, 2048, 2049,
             2050, 2051, 2052, 2053, 2054, 2055, 2056, 2057, 2058, 2059]
        )

    def test_end(self):
        self.assertEqual(
            self.modelTestEnd.getYearRange(),
            [2080, 2081, 2082, 2083, 2084, 2085, 2086, 2087, 2088, 2089,
             2090, 2091, 2092, 2093, 2094, 2095, 2096, 2097, 2098, 2099]
        )

    ## TODO: For each model run a set of tests that will check the output of loading is the correct shape

class TestAddStatistic(TestModelStatistics):
    def test_average(self):

        statistic = {
            "stat_type": "average",
            "time_freq": "month",
            "function": "mean"
        }
        self.modelTest.addStatistic(statistic)
        self.assertIn(ib.getStatStr(statistic), self.modelTest.getAvailableStats())

        os.remove(self.modelTest.getFilePath("one") / 'statistics.nc')

    def test_threshold(self):
        statistic = {
            "stat_type": "threshold",
            'var': 'prec',
            "threshold": 0.2541,
            "above": False,
            "time_freq": "month",
        }
        self.modelTestMid.addStatistic(statistic)
        self.assertIn(ib.getStatStr(statistic), self.modelTestMid.getAvailableStats())
        ## Cleanup
        os.remove(self.modelTestMid.getFilePath("one") / 'statistics.nc')

    def test_addTwoStats(self):
        file_path = self.modelTestEnd.getFilePath("one") / 'statistics.nc'

        if os.path.exists(file_path):
            os.remove(file_path)

        statistic = {
            "stat_type": "threshold",
            'var': 'prec',
            "threshold": 0.2541,
            "above": False,
            "time_freq": "month",
        }
        self.modelTestEnd.addStatistic(statistic)
        self.assertIn(ib.getStatStr(statistic), self.modelTestEnd.getAvailableStats())
        statistic = {
            "stat_type": "average",
            "time_freq": "month",
            "function": "mean"
        }
        self.modelTestEnd.addStatistic(statistic)
        self.assertIn(ib.getStatStr(statistic), self.modelTestEnd.getAvailableStats())
        ## Cleanup
        os.remove(file_path)
