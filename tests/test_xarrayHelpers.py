from unittest import TestCase
from helpers import xarrayHelpers as xrh
import xarray as xr
import pathlib


class TestXarrayHelpers(TestCase):
    def setUp(self):
        self.curpath = pathlib.Path(__file__).parent.absolute()
        self.grid = xr.open_dataset(self.curpath / 'test_data/test_output.nc')
        self.listofvars = ["something", "Something else"]
        self.shapefile = self.curpath / "test_data/candidate_cities_wgs1984/candidate_cities_wgs1984.shp"
        self.shapefileId = 'FEATURE_NA'


class TestTabularOutput(TestXarrayHelpers):

    def test_tabular_output_max(self):
        ret = xrh.tabularOutput(
            self.grid,
            ['year_mean_prec'],
            self.shapefile,
            self.shapefileId,
            'max'
        )

        self.assertAlmostEqual(
            ret['year_mean_prec'][2],
            2.229865312576294
        )

    def test_error_novars(self):
        with self.assertWarnsRegex(
                UserWarning,
                r'List of vars is empty. Please provide a variable') as cm:
            xrh.tabularOutput(
                self.grid,
                [],
                self.shapefile,
                self.shapefileId,
            )

    def test_error_valnotin(self):
        with self.assertWarnsRegex(
                UserWarning,
                r'ERROR: One of your vars is not in the grid') as cm:
            xrh.tabularOutput(
                self.grid,
                ['year_mean_prec', 'something false'],
                self.shapefile,
                self.shapefileId,
            )

    def test_year_func(self):
        ret = xrh.tabularOutput(
            self.grid,
            ['year_mean_prec'],
            self.shapefile,
            self.shapefileId,
        )
        self.assertListEqual(
            [self.shapefileId, 'computed_area', 'year_mean_prec'],
            list(ret.columns)
        )

    def test_month_func(self):
        ret = xrh.tabularOutput(
            self.grid,
            ['year_mean_prec', 'month_mean_prec'],
            self.shapefile,
            self.shapefileId,
        )
        self.assertListEqual(
            [self.shapefileId,
             'computed_area',
             'year_mean_prec',
             'month_mean_prec_1',
             'month_mean_prec_2',
             'month_mean_prec_3',
             'month_mean_prec_4',
             'month_mean_prec_5',
             'month_mean_prec_6',
             'month_mean_prec_7',
             'month_mean_prec_8',
             'month_mean_prec_9',
             'month_mean_prec_10',
             'month_mean_prec_11',
             'month_mean_prec_12'
             ],
            list(ret.columns)
        )

    def test_year_modelYear(self):
        cur_grid = xr.open_dataset(self.curpath / 'test_data/model_year_test.nc')
        ret = xrh.tabularOutput(
            cur_grid,
            ['year_mean_prec'],
            self.shapefile,
            self.shapefileId,
            modelTime=True
        )

        print(ret)
