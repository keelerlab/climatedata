from unittest import TestCase
from classes.reportGenerator import ReportGenerator
import configparser
from pathlib import Path



class TestReportGenerator(TestCase):
    def setUp(self):
        config = configparser.ConfigParser()
        self.curpath = Path(__file__).parent.absolute()
        config.read( self.curpath / '../config.ini')
        self.test = ReportGenerator(
            config['file_locations']['lccmr'],
            config['file_locations']['stats'],
            ['bcc-csm1-1', 'CCSM4', 'CNRM-CM5', 'GFDL-ESM2M', 'IPSL-CM5A-LR']
        )

class TestPrints(TestReportGenerator):
    def test_self_print(self):
        self.test
