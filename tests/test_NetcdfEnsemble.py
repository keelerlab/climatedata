from unittest import TestCase
from classes.NetcdfEnsemble import NetcdfEnsemble
import configparser
import pathlib


class TestNetcdfEnsemble(TestCase):
    def setUp(self):
        config = configparser.ConfigParser()
        self.curpath = pathlib.Path(__file__).parent.absolute()
        config.read(self.curpath / '../config.ini')
        self.netEnsemble = NetcdfEnsemble(
            config['file_locations']['lccmr'],
            config['file_locations']['stats'],
            ['bcc-csm1-1', 'CCSM4'],
            'MID',
            'RCP4.5'
        )

class TestInit(TestNetcdfEnsemble):

    def test_models_set(self):
        self.assertEqual(
            self.netEnsemble.models[0].model,
            'bcc-csm1-1')

        self.assertEqual(
            self.netEnsemble.models[1].model,
            'CCSM4')

    def test_models_wrong(self):
        with self.assertRaises(Exception) as context:
            NetcdfEnsemble('location','models')

    def test_models_empty(self):
        with self.assertRaises(Exception) as context:
            NetcdfEnsemble('location',[])